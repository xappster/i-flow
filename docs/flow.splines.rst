flow.splines package
====================

Submodules
----------

flow.splines.cubic module
-------------------------

.. automodule:: flow.splines.cubic
   :members:
   :undoc-members:
   :show-inheritance:

flow.splines.linear module
--------------------------

.. automodule:: flow.splines.linear
   :members:
   :undoc-members:
   :show-inheritance:

flow.splines.quadratic module
-----------------------------

.. automodule:: flow.splines.quadratic
   :members:
   :undoc-members:
   :show-inheritance:

flow.splines.rational\_quadratic module
---------------------------------------

.. automodule:: flow.splines.rational_quadratic
   :members:
   :undoc-members:
   :show-inheritance:

flow.splines.spline module
--------------------------

.. automodule:: flow.splines.spline
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: flow.splines
   :members:
   :undoc-members:
   :show-inheritance:
