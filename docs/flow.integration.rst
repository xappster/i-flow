flow.integration package
========================

Submodules
----------

flow.integration.couplings module
---------------------------------

.. automodule:: flow.integration.couplings
   :members:
   :undoc-members:
   :show-inheritance:

flow.integration.divergences module
-----------------------------------

.. automodule:: flow.integration.divergences
   :members:
   :undoc-members:
   :show-inheritance:

flow.integration.integrator module
----------------------------------

.. automodule:: flow.integration.integrator
   :members:
   :undoc-members:
   :show-inheritance:

   .. autoclass:: Integrator
      :members:

flow.integration.sinkhorn module
--------------------------------

.. automodule:: flow.integration.sinkhorn
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: flow.integration
   :members:
   :undoc-members:
   :show-inheritance:
